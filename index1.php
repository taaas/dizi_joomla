<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app  = JFactory::getApplication();
$user = JFactory::getUser();

// Output as HTML5
$this->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if ($task === 'edit' || $layout === 'form')
{
    $fullWidth = 1;
}
else
{
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add template js
JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));

// Add html5 shiv
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

// Add Stylesheets
JHtml::_('stylesheet', 'template.css', array('version' => 'auto', 'relative' => true));

// Use of Google Font
if ($this->params->get('googleFont'))
{
    JHtml::_('stylesheet', 'https://fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
    $this->addStyleDeclaration("
	h1, h2, h3, h4, h5, h6, .site-title {
		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
	}");
}

// Template color
if ($this->params->get('templateColor'))
{
    $this->addStyleDeclaration('
	body.site {
		border-top: 3px solid ' . $this->params->get('templateColor') . ';
		background-color: ' . $this->params->get('templateBackgroundColor') . ';
	}
	a {
		color: ' . $this->params->get('templateColor') . ';
	}
	.nav-list > .active > a,
	.nav-list > .active > a:hover,
	.dropdown-menu li > a:hover,
	.dropdown-menu .active > a,
	.dropdown-menu .active > a:hover,
	.nav-pills > .active > a,
	.nav-pills > .active > a:hover,
	.btn-primary {
		background: ' . $this->params->get('templateColor') . ';
	}');
}

// Check for a custom CSS file
JHtml::_('stylesheet', 'user.css', array('version' => 'auto', 'relative' => true));

// Check for a custom js file
JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Adjusting content width
$position7ModuleCount = $this->countModules('position-7');
$position8ModuleCount = $this->countModules('position-8');

if ($position7ModuleCount && $position8ModuleCount)
{
    $span = 'span6';
}
elseif ($position7ModuleCount && !$position8ModuleCount)
{
    $span = 'span9';
}
elseif (!$position7ModuleCount && $position8ModuleCount)
{
    $span = 'span9';
}
else
{
    $span = 'span12';
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
    <jdoc:include type="head" />
<!--    CSS-->
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/csstemplate.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.css.map” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap.min.css.map” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.css.map” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.min.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-grid.min.css.map” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.css.map” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.min.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/bootstrap-reboot.min.css.map” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/dizi.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/dizi.less” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/dizi-font.css” type=“text/css” />
    <link rel=“stylesheet” href=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/dizi-font.min.css” type=“text/css” />
<!--jquery-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<!--    JS-->
    <script src=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/bootstrap.min.js”></script>
    <script src=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/ie-emulation-modes-warning.js”></script>
    <script src=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/ie10-viewport-bug-workaround.js”></script>
    <script src=“<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/popper.min.js”></script>


</head>
<!--body-->
<body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '')
    . ($this->direction === 'rtl' ? ' rtl' : '');
?>">



<!--Footer-->
<footer class="py-4">
    <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
        <div class="row">
            <jdoc:include type="modules" name="footer" style="none" />
            <div class="col-12 col-md-9 d-none d-md-block"> <a href="mailto:info@dizi.lt" class="email d-block">info@dizi.lt</a>
                <div class="d-inline-block pr-5"> UAB â€žDIZIâ€?
                    <br>Adresas: Gedimino pr. 50, 01110 Vilnius
                    <br>Telefonas: 8 5 2624875 </div>
                <div class="d-inline-block"> UAB â€žDIZIâ€?
                    <br>Adresas: Gedimino pr. 50, 01110 Vilnius
                    <br>Telefonas: 8 5 2624875 </div>
            </div>
            <div class="col-12 col-md-3 text-md-right text-center">
                <?php echo JText::_('TPL_DIZI_JOOMLA_BACKTOTOP'); ?>

                &copy; Dizi 2001-<?php echo date('Y'); ?> <?php echo $sitename; ?> </div>
        </div>
    </div>
</footer>


</body>
</html>
